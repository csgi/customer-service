'use strict';

const express = require('express');
const customersRouter = require('./routes/customers');

// Constants
const PORT = 3000;
const HOST = '0.0.0.0';

// App
const app = express();

app.get('/', (_, res) => { res.send('Hello World!')});
app.use('/customers', customersRouter);

app.listen(PORT, HOST, () => {
  console.log(`Running on http://${HOST}:${PORT}`);
});

module.exports = app;
