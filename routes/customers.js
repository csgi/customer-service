var express = require('express');
var router = express.Router();

/* GET customers listing. */
router.get('/', function(req, res, next) {
  console.log('The endpoint was reached.');
  res.json([
    {
      id: 1,
      name: 'Pedro Brás',
      age: 38
    },
    {
      id: 2,
      name: 'Marta Oliveira',
      age: 39
    },
    {
      id: 3,
      name: 'Francisco Brás',
      age: 2
    }
  ]);
});

module.exports = router;
